<?php

// [Section] Objects as Variables

$buildingObj = (object) [
	'name' => 'Caswyn Building',
	'floors' => 8,
	'address' => (object) [
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];


// [Section] Objects from Classes

class Building {

	// Properties
	public $name;
	public $floors;
	public $address;

	//Constructor - a constructor is used during the creation of an object.
	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//Methods
	public function printName() {
		return "The name of the building is $this->name";
	}

}




/*
	Mini-Activity:
		1. Create an Animal class with 3 properties.
		2. Define a parametarized constructor for this class.
		3. Define a function for printing the animal name, family, gender.
		4. Use this class to instantiate a dog and a cat.

*/

class Animal {

	public $name;
	public $family;
	public $gender;


	public function __construct($name, $family, $gender) {
		$this->name = $name;
		$this->family = $family;
		$this->gender = $gender;
	}

	public function printName() {
		return "The name of this animal is $this->name";
	}

	public function printFamily() {
		return "The family of this animal is $this->family";
	}

	public function printGender() {
		return "The gender of this animal is $this->gender";
	}

}

$cat = new Animal("Brithney", "Felidae", "Female");
$dog = new Animal("Neomi", "Canidae", "Female");


// [Section] Inheritance and Polymorphism

class Condominium extends Building {

	// $name, $floors and $address are inherited in this class. We can still add additional properties for the child class.
	// It means that the Condominium class also have name, floors and address, just like a regular building.

	// The printName() function of this class overrides the behavior of the printName() from the Building class.
	public function printName() {
		return "The name of the condominium is $this->name";
	}

}

// Building class is the parent class, while the Condominium class is the child class.

// The constructor method is automatically executed when the "new Building" or the "new Condominium" is used.

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condominium', 5, 'Buendia Avenue, Makati City, Philippines');